#variables.tf (template from lab 13)

variable "container_name" {
  description = "Value of the name for the Docker container"
  # basic types include string, number and bool
  type    = string
  default = "AltaResearchWebService"
}

variable "internal_port" {
  description = "Value of the internal port"
  type = number
  default = 9876
}

variable "external_port" {
  description = "Value of the external port"
  type = number
  default = 5432
}
